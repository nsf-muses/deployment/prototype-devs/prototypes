# Deployment prototypes

This repo contains prototype of deployments, including Helm charts, ArgoCD application definitions, Kubernetes manifests, as well as source code and associated Dockerfiles.
