{{/*
Env vars list follows Docker Compose example at
https://github.com/indico/indico-containers/blob/master/indico.env.sample
*/}}
{{- define "indico.env" -}}
- name: SERVICE_PROTOCOL
  value: "https"
- name: PGHOST
  value: "{{ .Values.postgresql.fullnameOverride }}"
- name: PGUSER
  value: "{{ .Values.postgresql.auth.username }}"
- name: PGDATABASE
  value: "{{ .Values.postgresql.auth.database }}"
- name: PGPORT
  value: "5432"
- name: PGPASSWORD
  valueFrom:
    secretKeyRef:
      name: indico-secrets
      key: postgres-password
- name: SMTP_SERVER
  value: ""
- name: SMTP_PORT
  value: ""
- name: SMTP_LOGIN
  value: ""
- name: SMTP_PASSWORD
  value: ""
- name: SMTP_USE_TLS
  value: ""
- name: INDICO_DEFAULT_TIMEZONE
  value: "America/Chicago"
- name: INDICO_DEFAULT_LOCALE
  value: "en_US"
- name: INDICO_ROUTE_OLD_URLS
  value: ""
- name: INDICO_CHECKIN_APP_CLIENT_ID
  value: ""
- name: INDICO_CUSTOMIZATION_DIR
  value: ""
- name: INDICO_CUSTOMIZATION_DEBUG
  value: ""
- name: INDICO_LOGO_URL
  value: ""
- name: REDIS_CACHE_URL
  value: "redis://{{ .Values.redis.fullnameOverride }}:6379/1"
- name: CELERY_BROKER
  value: "redis://{{ .Values.redis.fullnameOverride }}:6379/0"
- name: C_FORCE_ROOT
  value: "true"
- name: USE_EXTERNAL_DB
  value: ""
- name: INDICO_STORAGE_DICT
  value: "{'default': 'fs:/opt/indico/archive'}"
- name: INDICO_AUTH_PROVIDERS
  value: "{}"
- name: INDICO_IDENTITY_PROVIDERS
  value: "{}"
- name: INDICO_LOCAL_IDENTITIES
  value: "yes"
- name: SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: indico-secrets
      key: secret-key
{{- end -}}
